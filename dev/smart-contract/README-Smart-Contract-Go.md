# README Simple Smart Contract on Go

## 1. Необходимые расширения VS Code

`WSL` - необходим для подключения к тестовой среде развернутой в Ubuntu

`Docker` - для взаимодействия с контейнерами через терминал. <i> После подлючения к виртуальной машине, необходимо установить расширение для виртуальной машины.</i>

## 2. Файлы смарт контракта

В папке `simple_chaincode_go` находятся исходники чейнкода, их необходимо скопировать на виртуальную машину


## 3. Применение смарт контракта

<i>Все дальнейшие действия производить через терминал VS Code или непосредственно через терминал WSL</i>

Необходимо скопировать файлы смарт контракта в контейнер docker:

```sh
docker cp /tmp/simple_chaincode_go/ cli_nbrb:tmp
```

Компиляция применяемого в дальнейшем чейнкода:

```sh
docker exec cli_nbrb peer lifecycle chaincode package simple_chaincode_go.tar.gz --label simple_chaincode_go_0.0.1 --lang golang --path /tmp/simple_chaincode_go/
```

Заходим в контейнер:

```sh
docker exec -it cli_nbrb bash
```

Применяем чейнкод:

```sh
peer lifecycle chaincode install simple_chaincode_go.tar.gz
```

В случае успеха, получаем следующее сообщение: 

```log
2024-02-21 06:22:47.161 UTC 0001 INFO [cli.lifecycle.chaincode] submitInstallProposal -> Installed remotely: response:<status:200 payload:"\nZsimple_chaincode_go_0.0.1:eef7454451a18b341ee3ced8bd0dac922916f16960930ced0654712a6f40da61\022\031simple_chaincode_go_0.0.1">

[cli.lifecycle.chaincode] submitInstallProposal -> Chaincode code package identifier: simple_chaincode_go_0.0.1:eef7454451a18b341ee3ced8bd0dac922916f16960930ced0654712a6f40da61
```

Необходимо запомнить и применить <i>Chaincode code package identifier</i>, в данном случае это <b>simple_chaincode_go_0.0.1:eef7454451a18b341ee3ced8bd0dac922916f16960930ced0654712a6f40da61</b>: 

```sh
export PACKAGE_ID=simple_chaincode_go_0.0.1:eef7454451a18b341ee3ced8bd0dac922916f16960930ced0654712a6f40da61
```

Теперь необходимо утвердить изменения:

```sh
peer lifecycle chaincode approveformyorg -o bisc.cbt.by:7050 --channelID cbtchannel --name simple_chaincode_go --version 0.0.1 --sequence 1 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem --package-id $PACKAGE_ID
```

В случае успеха, получаем следующее сообщение:

```log
2024-02-21 07:10:42.274 UTC 0001 INFO [chaincodeCmd] ClientWait -> txid [035b28959f39161344f42287f89da81bbca0dbdf07d3d079acb1de8b23a6564d] committed with status (VALID) at peer0.nbrb.cbt.by:7051
```

Теперь нужно утвердить измения на большинстве нодов. Прописываем <b>exit</b>, происходит выход из управляющего cli в корневой раздел.

### Установим чейнкод для ноды банка `Alfa`:

Необходимо скопировать файлы смарт-контракта в контейнер `alfa`:
```sh
docker cp /tmp/simple_chaincode_go/ cli_alfa:tmp
```
Компиляция применяемого в дальнейшем чейнкода:
```sh
docker exec cli_alfa peer lifecycle chaincode package simple_chaincode_go.tar.gz --label simple_chaincode_go_0.0.1 --lang golang --path /tmp/simple_chaincode_go/ 
```

Заходим в docker-контейнер `alfa`:
```sh
docker exec -it cli_alfa bash
```

Применяем чейнкод:
```sh
peer lifecycle chaincode install simple_chaincode_go.tar.gz
```

Необходимо применить <i>Chaincode code package identifier</i>, в данном случае это <b>simple_chaincode_go_0.0.1:eef7454451a18b341ee3ced8bd0dac922916f16960930ced0654712a6f40da61</b>: 

```sh
export PACKAGE_ID=simple_chaincode_go_0.0.1:eef7454451a18b341ee3ced8bd0dac922916f16960930ced0654712a6f40da61
```

Утверждение изменений:
```sh
peer lifecycle chaincode approveformyorg -o bisc.cbt.by:7050 --channelID cbtchannel --name simple_chaincode_go --version 0.0.1 --sequence 1 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem --package-id $PACKAGE_ID
```

### По аналогии можно сделать для нод и других банков!

Для остальных банков мы сначала просто утверждаем:

`Sber`:

```sh
docker exec -it cli_sber bash
```

```sh
export PACKAGE_ID=simple_chaincode_go_0.0.1:eef7454451a18b341ee3ced8bd0dac922916f16960930ced0654712a6f40da61
```

```sh
peer lifecycle chaincode approveformyorg -o bisc.cbt.by:7050 --channelID cbtchannel --name simple_chaincode_go --version 0.0.1 --sequence 1 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem --package-id $PACKAGE_ID
```
`Vtb`:

```sh
docker exec -it cli_vtb bash
```

```sh
export PACKAGE_ID=simple_chaincode_go_0.0.1:eef7454451a18b341ee3ced8bd0dac922916f16960930ced0654712a6f40da61
```

```sh
peer lifecycle chaincode approveformyorg -o bisc.cbt.by:7050 --channelID cbtchannel --name simple_chaincode_go --version 0.0.1 --sequence 1 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem --package-id $PACKAGE_ID
```

В текущем исполнении чейнкод будет запущен только для `nbrb` и `Afla` послке утверждения большинства и коммита:

```sh
peer lifecycle chaincode commit -o bisc.cbt.by:7050 --channelID cbtchannel --name simple_chaincode_go --version 0.0.1 --sequence 1 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem --peerAddresses peer0.nbrb.cbt.by:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/nbrb.cbt.by/users/Admin@nbrb.cbt.by/tls/ca.crt --peerAddresses peer0.bank-vtb.cbt.by:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-vtb.cbt.by/users/Admin@bank-vtb.cbt.by/tls/ca.crt --peerAddresses peer0.bank-sber.cbt.by:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-sber.cbt.by/users/Admin@bank-sber.cbt.by/tls/ca.crt --peerAddresses peer0.bank-alfa.cbt.by:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-alfa.cbt.by/users/Admin@bank-alfa.cbt.by/tls/ca.crt
```

В случае успеха коммита:

```log
2024-02-21 09:17:44.195 UTC 0001 INFO [chaincodeCmd] ClientWait -> txid [86f6b19e569f234609f9e95270a4de5f5df09247654df39b8bafbbb164c0ddd6] committed with status (VALID) at peer0.nbrb.cbt.by:7051
2024-02-21 09:17:44.209 UTC 0002 INFO [chaincodeCmd] ClientWait -> txid [86f6b19e569f234609f9e95270a4de5f5df09247654df39b8bafbbb164c0ddd6] committed with status (VALID) at peer0.bank-vtb.cbt.by:7051
2024-02-21 09:17:44.212 UTC 0003 INFO [chaincodeCmd] ClientWait -> txid [86f6b19e569f234609f9e95270a4de5f5df09247654df39b8bafbbb164c0ddd6] committed with status (VALID) at peer0.bank-alfa.cbt.by:7051
2024-02-21 09:17:44.212 UTC 0004 INFO [chaincodeCmd] ClientWait -> txid [86f6b19e569f234609f9e95270a4de5f5df09247654df39b8bafbbb164c0ddd6] committed with status (VALID) at peer0.bank-sber.cbt.by:7051
```

Для просмотра установленных чейнкодов:

```sh
peer lifecycle chaincode queryinstalled
```

Успешный результат:

```log
Installed chaincodes on peer:
Package ID: simple_chaincode_go_0.0.1:eef7454451a18b341ee3ced8bd0dac922916f16960930ced0654712a6f40da61, Label: simple_chaincode_go_0.0.1
```
--- 

Если необходиммо запустить чейнкод на другой ноде, например, для `Vtb`:

```sh
docker cp /tmp/simple_chaincode_go/ cli_vtb:tmp
```

```sh
docker exec cli_vtb peer lifecycle chaincode package simple_chaincode_go.tar.gz --label simple_chaincode_go_0.0.1 --lang golang --path /tmp/simple_chaincode_go/
```

```sh
docker exec -it cli_vtb bash
```

```sh
peer lifecycle chaincode install simple_chaincode_go.tar.gz
```

---
## 4. Проверка работоспособности чейнкода

В этом простом чейнкоде реализовано 2 функции: `GetVersion` и `SetVersion`. 

Для проверки работоспособности, необоходимо перейти внутрь cli, на которой установлен необходимый чейнкод. В данном случае - `nbrb`:

```sh
docker exec -it cli_nbrb bash
```

Для выполнения функции `GetVersion`:

```sh
peer chaincode invoke -o bisc.cbt.by:7050 --channelID cbtchannel --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem  --name simple_chaincode_go -c '{"function": "GetVersion", "Args":[]}'
```

В случае успеха, получится примерно следующий результат:

```log
2024-02-22 09:19:19.380 UTC 0001 INFO [chaincodeCmd] chaincodeInvokeOrQuery -> Chaincode invoke successful. result: status:200 payload:"0.0.1"
```

`Значение версии по умолчанию - "0.0.1"`

Для выполнения функции `SetVersion`:

```sh
peer chaincode invoke -o bisc.cbt.by:7050 --channelID cbtchannel --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem  --name simple_chaincode_go -c '{"function": "SetVersion", "Args":["0.0.2"]}'
```

В случае успеха, получится примерно следующий результат:

```log
2024-02-22 09:20:05.741 UTC 0001 INFO [chaincodeCmd] chaincodeInvokeOrQuery -> Chaincode invoke successful. result: status:200
```

Для проверки, что задано новое значение версии, следует вызвать функцию `GetVersion`, и получить примерно следующий результат:

```log
2024-02-22 09:20:08.795 UTC 0001 INFO [chaincodeCmd] chaincodeInvokeOrQuery -> Chaincode invoke successful. result: status:200 payload:"0.0.2"
```

## Полезные ссылки

1. Туториал, по создания смарт-контракта, используя fabric-contract-api-go: https://github.com/hyperledger/fabric-contract-api-go/tree/main/tutorials

2. Пример смарт-контракта на go:  https://github.com/hyperledger/fabric-samples/blob/main/asset-transfer-basic/chaincode-go/chaincode/smartcontract.go

3. Пример деплоя чейнкода с официальной документации: https://hyperledger-fabric.readthedocs.io/en/release-2.0/deploy_chaincode.html