package main

import (
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

var version string = "0.0.1"

type SimpleContract struct {
	contractapi.Contract
}

func (sc *SimpleContract) GetVersion(ctx contractapi.TransactionContextInterface) string {
	return version
}

func (sc *SimpleContract) SetVersion(ctx contractapi.TransactionContextInterface, newVersion string) {
	version = newVersion
}
