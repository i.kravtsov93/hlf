'use strict';


const { Contract } = require('fabric-contract-api');

const accountObjType = "Account";

function _isNaturalNumber(n) {
    n = n.toString(); // force the value incase it is not
    var n1 = Math.abs(n),
        n2 = parseInt(n, 10);
    return !isNaN(n1) && n2 === n1 && n1.toString() === n;
}

class DigitalCurrencyCore extends Contract {

    async getVersion(ctx) {
        return "0.1.12";
    }

    _getCompositeKeyClientBelarus(ctx, client_type, client_unique_ID) {
        let key_prefix = "CLIENT";
        let key_client_type = "UNKNOWN";

        console.log("DEBUG: client_type: ", client_type);
        console.log("DEBUG: client_unique_ID: ", client_unique_ID);

        switch (client_type) {
            case "person":
                key_client_type = "PERSON_BY";
                break;
            case "org":
                key_client_type = "ORG_BY";
                break;
            default:
                throw new Error('{result: "error", description: "unknown client_type"}');
        }

        if ("PERSON_BY" == key_client_type) {
            if (client_unique_ID.length != 14)
                throw new Error('{result: "error", description: "client_unique_ID for BY citizen must have 14 length"}');
        }
        else if ("ORG_BY" == key_client_type) {
            if (client_unique_ID.length != 9)
                throw new Error('{result: "error", description: "client_unique_ID for BY organisation must consist of 9 digits"}');
        }
        else {
            throw new Error('{result: "error", description: "unexpected client_type"}');
        }


        const key = key_prefix + '_' + key_client_type; // + '_' + client_unique_ID;

        console.log("DEBUG: const compositeKey = ctx.stub.createCompositeKey(key, [client_unique_ID]);");
        const compositeKey = ctx.stub.createCompositeKey(key, [client_unique_ID]);
        return compositeKey;
    }

    async addClientBelarus(ctx, client_type, client_unique_ID) {
        const compositeKey = this._getCompositeKeyClientBelarus(ctx, client_type, client_unique_ID);

        let client_value = {};

        switch (client_type) {
            case "person":
                client_value = {
                    client_type: "PERSON",
                    country: "BY",
                    personal_number: client_unique_ID
                };
                break;
            case "org":
                client_value = {
                    client_type: "ORG",
                    country: "BY",
                    UNP: client_unique_ID
                };
                break;
            default:
                throw new Error('{result: "error", description: "unknown client_type"}');
        }

        console.log("DEBUG: await ctx.stub.putState(compositeKey, Buffer.from(JSON.stringify(client_value)));");
        await ctx.stub.putState(compositeKey, Buffer.from(JSON.stringify(client_value)));
    }

    async openNewAccount(ctx, client_type, client_unique_ID) {
        let accountNumber = "";
        switch (client_type) {
            case "person":
                accountNumber = "BY00" + "NBRB" + "DC01" + client_unique_ID + "00"; // TODO: uppercase and translate to ENG letters
                break;
            case "org":
                accountNumber = "BY00" + "NBRB" + "DC01" + client_unique_ID + "000" + "0000";
                break;
            default:
                throw new Error('{result: "error", description: "unknown client_type"}');
        }

        const clientCompositeKey = this._getCompositeKeyClientBelarus(ctx, client_type, client_unique_ID);

        let account = {
            number: accountNumber,
            balance: 0,
            client: clientCompositeKey
        };

        console.log("DEBUG: const accountCompositeKey = ctx.stub.createCompositeKey(\"ACCOUNT\", [accountNumber]);");
        const accountCompositeKey = ctx.stub.createCompositeKey("ACCOUNT", [accountNumber]);

        console.log("DEBUG: await ctx.stub.putState(accountCompositeKey, Buffer.from(JSON.stringify(account)));");
        await ctx.stub.putState(accountCompositeKey, Buffer.from(JSON.stringify(account)));        
    }











    async initAccount(ctx, id, balance) {
        if (!ctx.clientIdentity.assertAttributeValue("init", "true")) {
            throw new Error(`you don't have permissions to initialize an account`);
        }

        const accountBalance = parseFloat(balance);
        if (accountBalance < 0) {
            throw new Error(`account balance cannot be negative`);
        }

        const account = {
            id: id,
            owner: this._getTxCreatorUID(ctx),
            balance: accountBalance
        }

        if (await this._accountExists(ctx, account.id)) {
            throw new Error(`the account ${account.id} already exists`);
        }

        await this._putAccount(ctx, account);
    }

    async setBalance(ctx, id, newBalance) {
        const newAccountBalance = parseFloat(newBalance);
        if (newAccountBalance < 0) {
            throw new Error(`account balance cannot be set to a negative value`);
        }

        let account = await this._getAccount(ctx, id);

        const txCreator = this._getTxCreatorUID(ctx);
        if (account.owner !== txCreator) {
            throw new Error(`unauthorized access: you can't change account that doesn't belong to you`);
        }

        account.balance = newAccountBalance;
        await this._putAccount(ctx, account);
    }

    async transfer(ctx, idFrom, idTo, amount) {
        const amountToTransfer = parseFloat(amount);
        if (amountToTransfer <= 0) {
            throw new Error(`amount to transfer cannot be negative`);
        }

        let accountFrom = await this._getAccount(ctx, idFrom);

        const txCreator = this._getTxCreatorUID(ctx);
        if (accountFrom.owner !== txCreator) {
            throw new Error(`unauthorized access: you can't change account that doesn't belong to you`);
        }

        let accountTo = await this._getAccount(ctx, idTo);

        if (accountFrom.balance < amountToTransfer) {
            throw new Error(`amount to transfer cannot be more than the current account balance`);
        }

        accountFrom.balance -= amountToTransfer
        accountTo.balance += amountToTransfer

        await this._putAccount(ctx, accountFrom);
        await this._putAccount(ctx, accountTo);
    }

    async listAccounts(ctx) {
        const txCreator = this._getTxCreatorUID(ctx);

        const iteratorPromise = ctx.stub.getStateByPartialCompositeKey(accountObjType, []);

        let results = [];
        for await (const res of iteratorPromise) {
            const account = JSON.parse(res.value.toString());
            if (account.owner === txCreator) {
                results.push(account);
            }
        }

        return JSON.stringify(results);
    }

    _getTxCreatorUID(ctx) {
        return JSON.stringify({
            mspid: ctx.clientIdentity.getMSPID(),
            id: ctx.clientIdentity.getID()
        });
    }

    async _accountExists(ctx, id) {
        const compositeKey = ctx.stub.createCompositeKey(accountObjType, [id]);
        const accountBytes = await ctx.stub.getState(compositeKey);
        return accountBytes && accountBytes.length > 0;
    }

    async _getAccount(ctx, id) {
        const compositeKey = ctx.stub.createCompositeKey(accountObjType, [id]);

        const accountBytes = await ctx.stub.getState(compositeKey);
        if (!accountBytes || accountBytes.length === 0) {
            throw new Error(`the account ${id} does not exist`);
        }

        return JSON.parse(accountBytes.toString());
    }

    async _putAccount(ctx, account) {
        const compositeKey = ctx.stub.createCompositeKey(accountObjType, [account.id]);
        await ctx.stub.putState(compositeKey, Buffer.from(JSON.stringify(account)));
    }
}

module.exports = DigitalCurrencyCore;