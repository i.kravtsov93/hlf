'use strict';

const digitalCurrency = require('./lib/digitalCurrencyCore');

module.exports.DigitalCurrency = digitalCurrency;
module.exports.contracts = [digitalCurrency];