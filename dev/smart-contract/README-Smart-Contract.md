## Для применения смарт контракта в тестовой сети:

1.  # Необходимы расширения VS Code
  WSL - необходим для подключения к тестовой среде развернутой в ubuntu 

  Docker - для взаимодействия с контейнерами через терминал. После подключения к виртуальной необходимо установить расширение для виртуальной машины.

2. # Файлы смарт контракта 
 
 В папке 'digital_currency_core' находятся исходники чейнкода, их необходимо скопировать на виртуальную машину. 

 Вот, примерный, результат:

![image.png](/.attachments/image-5648dcea-c554-4d67-8112-f84d7377c6e3.png)

 3. # Применение смарт контракта

 Все дальнейшие действия производить через терминал VS Code.

 Необходимо скопировать файлы смарт контракта в контейнер докера:

docker cp /tmp/digital_currency_core/ cli_nbrb:tmp

    /tmp/digital_currency_core/ - путь к файлам.

 Успешный результат:
Successfully copied 12.8kB to cli_nbrb:tmp

 Компиляция применяемого в дальнейшем чейнкода:
 
docker exec cli_nbrb peer lifecycle chaincode package digital_currency_core.tar.gz --label digital_currency_core_0.1.1 --lang node --path /tmp/digital_currency_core/


 Заходим в контейнер 
 
docker exec -it cli_nbrb bash

 Применяем чейнкод:
 
peer lifecycle chaincode install digital_currency_core.tar.gz
  
  Пример результата 

  2024-02-15 13:55:42.490 UTC 0001 INFO [cli.lifecycle.chaincode] submitInstallProposal -> Installed remotely: response:<status:200 payload:"\n\\digital_currency_core_0.1.1:379bce4d9d7934d8761b32ed161b863eee844bb3179367ba78f5340b5ff946b2\022\033digital_currency_core_0.1.1" > 
2024-02-15 13:55:42.490 UTC 0002 INFO [cli.lifecycle.chaincode] submitInstallProposal -> Chaincode code package identifier: digital_currency_core_0.1.1:379bce4d9d7934d8761b32ed161b863eee844bb3179367ba78f5340b5ff946b2

В результаты у вас появится новый IMAGE от докер контейнера, т.е. ваш смарт контракт превратится в докер, который еще не запущен.
![image.png](/.attachments/image-89b36aa2-2146-4cf8-86d3-19ca849a5232.png)

Необходимо запомнить и применить Chaincode code package identifier, в нашем случае это "379bce4d9d7934d8761b32ed161b863eee844bb3179367ba78f5340b5ff946b2".

export PACKAGE_ID=digital_currency_core_0.1.1:379bce4d9d7934d8761b32ed161b863eee844bb3179367ba78f5340b5ff946b2

Теперь необходимо утвердить это изменение:

peer lifecycle chaincode approveformyorg -o bisc.cbt.by:7050 --channelID cbtchannel --name digital_currency_core --version 0.1.1 --sequence 1 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem --package-id $PACKAGE_ID

Успешный результат 
2024-02-16 14:24:26.288 UTC 0001 INFO [chaincodeCmd] ClientWait -> txid [ad9fe16f3a6391829c75cae1124eec67d696b06088fdd7949ad8077495854c1f] committed with status (VALID) at peer0.nbrb.cbt.by:7051

![Alt text](./Smart-Contract/Nbrb_Terminal.png)


Теперь нужно утвердить изменение на большинстве нодов. 
 Прописывая exit происходит выход из управляющего cli в корневой раздел (dbyn@48-1222:~/infra/dev$)

alfa - для этой ноды банка тоже сразу установим чейнкод 

docker cp /tmp/digital_currency_core/ cli_alfa:tmp

docker exec cli_alfa peer lifecycle chaincode package digital_currency_core.tar.gz --label digital_currency_core_0.1.1 --lang node --path /tmp/digital_currency_core/

docker exec -it cli_alfa bash

peer lifecycle chaincode install digital_currency_core.tar.gz

Внимание - укажите правильный хэш, кстати он совпадет с тем, который был на ноде nbrb
export PACKAGE_ID=digital_currency_core_0.1.1:379bce4d9d7934d8761b32ed161b863eee844bb3179367ba78f5340b5ff946b2

peer lifecycle chaincode approveformyorg -o bisc.cbt.by:7050 --channelID cbtchannel --name digital_currency_core --version 0.1.1 --sequence 1 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem --package-id $PACKAGE_ID

![Alt text](./Smart-Contract/Alfa_Terminal.png)

Для остальных банков мы сначала просто утверждаем, но можно повторить все операции и тогда чейнкод установится на  ноде.
sber

docker exec -it cli_sber bash

export PACKAGE_ID=digital_currency_core_0.1.1:379bce4d9d7934d8761b32ed161b863eee844bb3179367ba78f5340b5ff946b2

peer lifecycle chaincode approveformyorg -o bisc.cbt.by:7050 --channelID cbtchannel --name digital_currency_core --version 0.1.1 --sequence 1 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem --package-id $PACKAGE_ID

Выполните команду
> exit

vtb

docker exec -it cli_vtb bash

export PACKAGE_ID=digital_currency_core_0.1.1:379bce4d9d7934d8761b32ed161b863eee844bb3179367ba78f5340b5ff946b2

peer lifecycle chaincode approveformyorg -o bisc.cbt.by:7050 --channelID cbtchannel --name digital_currency_core --version 0.1.1 --sequence 1 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem --package-id $PACKAGE_ID

![Alt text](./Smart-Contract/Sber_Vtb_Terminal.png)

**Проверка готовности к запуску чейнкода**
Изнутри из контейнера любого банка (пира) можно запустить команду **checkcommitreadiness**:

peer lifecycle chaincode checkcommitreadiness -o bisc.cbt.by:7050 --channelID cbtchannel --name digital_currency_core --version 0.1.1 --sequence 1 --tls --cafile /var/hyperledger/orderer/msp/tlscacerts/tlsca.cbt.by-cert.pem

Получится примерно такой результат, с описанием перечня организаций готовых к запуску:
![image.png](/.attachments/image-dfc8d309-33bb-4970-818f-e7ee302d3d34.png)

В текущем исполнении чейнкод будет запущен только для nbrb и Alfa после утверждения большинства и коммита.
peer lifecycle chaincode commit -o bisc.cbt.by:7050 --channelID cbtchannel --name digital_currency_core --version 0.1.1 --sequence 1 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cbt.by/msp/tlscacerts/tlsca.cbt.by-cert.pem  --peerAddresses peer0.nbrb.cbt.by:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/nbrb.cbt.by/users/Admin@nbrb.cbt.by/tls/ca.crt --peerAddresses peer0.bank-vtb.cbt.by:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-vtb.cbt.by/users/Admin@bank-vtb.cbt.by/tls/ca.crt --peerAddresses peer0.bank-sber.cbt.by:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-sber.cbt.by/users/Admin@bank-sber.cbt.by/tls/ca.crt --peerAddresses peer0.bank-alfa.cbt.by:7051 --tlsRootCertFiles /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-alfa.cbt.by/users/Admin@bank-alfa.cbt.by/tls/ca.crt

![Alt text](./Smart-Contract/Commit_Result.png)

Если необходимо запустить чейнкод на другой ноде необходимо, например для vtb 

docker cp /tmp/digital_currency_core/ cli_vtb:tmp

docker exec cli_vtb peer lifecycle chaincode package digital_currency_core.tar.gz --label digital_currency_core_0.1.1 --lang node --path /tmp/digital_currency_core/

docker exec -it cli_vtb bash

peer lifecycle chaincode install digital_currency_core.tar.gz

![Alt text](./Smart-Contract/Vtb_AddContainer.png)

Получить список установленных чейнкодов:
peer lifecycle chaincode queryinstalled

![Alt text](./Smart-Contract/Peer_Queryinstalled.png)






