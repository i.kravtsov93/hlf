# Set env vars
export CHANNEL_NAME=cbtchannel

#docker compose -f docker-compose.yml down

docker compose -f docker-compose.yml up -d


# Wait for Hyperledger Fabric to start
# In case of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
export FABRIC_START_TIMEOUT=5
sleep ${FABRIC_START_TIMEOUT}

ORDERER_TLS_CA=`docker exec cli_nbrb  env | grep ORDERER_TLS_CA | cut -d'=' -f2`
docker exec cli_nbrb peer channel create -o bisc.cbt.by:7050 -c $CHANNEL_NAME -f /etc/hyperledger/configtx/$CHANNEL_NAME.tx --tls --cafile $ORDERER_TLS_CA
#echo "Channel with name $CHANNEL_NAME created"

# Join channel nbrb
docker exec -e CORE_PEER_ADDRESS=peer0.nbrb.cbt.by:7051 \
    -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/nbrb.cbt.by/peers/peer0.nbrb.cbt.by/tls/server.crt \
    -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/nbrb.cbt.by/peers/peer0.nbrb.cbt.by/tls/server.key \
    -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/nbrb.cbt.by/peers/peer0.nbrb.cbt.by/tls/ca.crt \
    cli_nbrb peer channel fetch 0 cbtchannel.pb -o bisc.cbt.by:7050 --tls --cafile $ORDERER_TLS_CA -c cbtchannel
docker exec cli_nbrb peer channel join -b cbtchannel.pb
#echo "Join channel nbrb - complete"

# Anchor peer - peer0.nbrb.cbt.by
docker exec cli_nbrb peer channel update -o bisc.cbt.by:7050 --tls --cafile $ORDERER_TLS_CA -c $CHANNEL_NAME -f /etc/hyperledger/configtx/nbrbMSPanchors.tx 

# Join channel alfa
docker exec -e CORE_PEER_ADDRESS=peer0.bank-alfa.cbt.by:7051 \
    -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-alfa.cbt.by/peers/peer0.bank-alfa.cbt.by/tls/server.crt \
    -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-alfa.cbt.by/peers/peer0.bank-alfa.cbt.by/tls/server.key \
    -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-alfa.cbt.by/peers/peer0.bank-alfa.cbt.by/tls/ca.crt \
    cli_alfa peer channel fetch 0 cbtchannel.pb -o bisc.cbt.by:7050 --tls --cafile $ORDERER_TLS_CA -c cbtchannel
docker exec cli_alfa peer channel join -b cbtchannel.pb
#echo "Join channel alfa - complete"

# Anchor peer - peer0.bank-alfa.cbt.by
docker exec cli_alfa peer channel update -o bisc.cbt.by:7050 --tls --cafile $ORDERER_TLS_CA -c $CHANNEL_NAME -f /etc/hyperledger/configtx/alfaMSPanchors.tx 

# Join channel sber
docker exec -e CORE_PEER_ADDRESS=peer0.bank-sber.cbt.by:7051 \
    -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-sber.cbt.by/peers/peer0.bank-sber.cbt.by/tls/server.crt \
    -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-sber.cbt.by/peers/peer0.bank-sber.cbt.by/tls/server.key \
    -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/nbank-sber.cbt.by/peers/peer0.bank-sber.cbt.by/tls/ca.crt \
    cli_sber peer channel fetch 0 cbtchannel.pb -o bisc.cbt.by:7050 --tls --cafile $ORDERER_TLS_CA -c cbtchannel
docker exec cli_sber peer channel join -b cbtchannel.pb
#echo "Join channel sber - complete"

# Anchor peer - peer0.bank-sber.cbt.by
docker exec cli_sber peer channel update -o bisc.cbt.by:7050 --tls --cafile $ORDERER_TLS_CA -c $CHANNEL_NAME -f /etc/hyperledger/configtx/sberMSPanchors.tx 

# Join channel vtb
docker exec -e CORE_PEER_ADDRESS=peer0.bank-vtb.cbt.by:7051 \
    -e CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-vtb.cbt.by/peers/peer0.bank-vtb.cbt.by/tls/server.crt \
    -e CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/bank-vtb.cbt.by/peers/peer0.bank-vtb.cbt.by/tls/server.key \
    -e CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/nbank-vtb.cbt.by/peers/peer0.bank-vtb.cbt.by/tls/ca.crt \
    cli_vtb peer channel fetch 0 cbtchannel.pb -o bisc.cbt.by:7050 --tls --cafile $ORDERER_TLS_CA -c cbtchannel
docker exec cli_vtb peer channel join -b cbtchannel.pb
#echo "Join channel vtb - complete"

# Anchor peer - peer0.bank-vtb.cbt.by
docker exec cli_vtb peer channel update -o bisc.cbt.by:7050 --tls --cafile $ORDERER_TLS_CA -c $CHANNEL_NAME -f /etc/hyperledger/configtx/vtbMSPanchors.tx 