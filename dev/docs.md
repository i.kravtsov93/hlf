## Для запуска тестовой сети:

# 1. Установка подсистемы Windows для Linux (WSL) производится в PowerShell:
```
wsl --install
```

![Alt text](images/image.png)

Система Windows спросит разрешение на внесение изменений на устройстве. Необходимо нажать кнопку "Да".

![Alt text](images/image-1.png)

Система Windows сново спросит разрешение на внесение изменений на устройстве. Нажимаем кнопку "Да".

![Alt text](images/image-2.png)


Дождаться окончания установки. По завершении будет рекомендация по перезагрузке системы. Необходимо перезагрузить систему Windows.

![Alt text](images/image-3.png)

Если в ходе установки или подключения к wsl будет ошибка "Please enable the Virtual Machine Platform Windows feature and ensure virtualization is enabled in the BIOS." то необходимо Включить виртуализацию в BIOS.

# 2. Запустить WSL.
После перезагрузки необходимо дождаться завершения установки и запуска Ubuntu (5-10 минут).

![Alt text](images/image-4.png)

После завершения установки необходимо ввести имя пользователя для UNIX системы (рекомендуемое имя: dbyn).

![Alt text](images/image-5.png)

Так же необходимо будет ввести пароль и его подтверждение (рекомендуемый пароль: 1qazXSW@).

![Alt text](images/image-6.png)

Если все заполненно правильно должна появиться командная строка и сообщение "Welcome to Ubuntu..."

![Alt text](images/image-7.png)

# 3. Установить в WSL [Git](https://git-scm.com/downloads):
```
sudo apt-get install git
```
Во время выполенния команды установки [Git](https://git-scm.com/downloads) необходимо будет ввести пароль используемый вами в пункте 2. Если все правильно, [Git](https://git-scm.com/downloads) будет успешно установлен.

![Alt text](images/image-8.png)

# 4. Клонировать репозиторий:
```
git clone http://cbt-dbr.cbt.by/Collection/DBR/_git/infra
```
Для выполнения клонирования репозитория нужно будет указать свои учетные данные домена(пример: логин user@cbt.by и пароль от доменной УЗ).

![Alt text](images/image-9.png)

При успешном клонировании репозитория будет выведена следующая информация:

![Alt text](images/image-10.png)

# 5. Запустить скрипт установки [Docker](https://www.docker.com/), который находится в репозитории:
Необходимо перейти в каталог dev репозитория infra и выдать право запуска скрипту install-docker.sh (возможно попросит ввод пароля созданого в пункте 2).
```
cd infra/dev/ && chmod +x install-docker.sh
```

![Alt text](images/image-11.png)

Выполнить скрипт install-docker.sh. В результате должен установиться Docker.
```
./install-docker.sh
```

![Alt text](images/image-12.png)

# 6. Перезагрузить WSL - выполнить в PowerShell:
Выключить Ubuntu командой ниже(выполнить в PowerShell). после сообщения об успешном завершении операции 
```
wsl --terminate Ubuntu
```

![Alt text](images/image-13.png)

После сообщения об успешном завершении операции, необходимо опять запустить Ubuntu (можно воспользоваться поиском).

![Alt text](images/image-14.png)

# 7. Запустите тестовой сети:
Для запуска тестовой сети скопируйте приведенные ниже команды и вставьте в командную строку Ubuntu (команды рекомендуется выполнять по одной).
```
cd infra/dev/
chmod +x bin/* generate.sh start.sh teardown.sh
./generate.sh
./start.sh
```
или
```
cd infra/dev/ && chmod +x bin/* generate.sh start.sh teardown.sh && ./generate.sh && ./start.sh
```
При успешной генерации конфигурации (если есть ошибки обратиться в УСАиБ):

![Alt text](images/image-15.png)

Необходимо будет подождать, пока скачаются Docker образы из сети интернет. Если ошибок, то канал успешно создан (если есть ошибки обратиться в УСАиБ):

![Alt text](images/image-16.png)

Для перезапуска сети выполните:
```
./teardown.sh && sleep 5 && ./start.sh
```

Для просмотра списка docker-контейнеров сети Hyperledger Fabric, их "доменных" имен и портов:
```
docker ps
```

TLS сертификат Ордерера находится по пути:
# /var/hyperledger/orderer/msp/tlscacerts/tlsca.cbt.by-cert.pem

# 8. Если необходимо удалить все данные и заново установить
Например поменялись какие-то файлы запуска сети. То выполните
![image.png](/.attachments/image-396e33d1-bfd2-4c0b-bb7a-ace9aa6a4b8c.png)

А после можете заново запустить Ubuntu. (Просто из меню Пуск / Ubuntu)