#!/bin/bash
#
# Copyright IBM Corp All Rights Reserved
#
# SPDX-License-Identifier: Apache-2.0
#

# Set env vars
export PATH=./bin:$PATH
export FABRIC_CFG_PATH=${PWD}
export CHANNEL_NAME=cbtchannel

# Remove previous crypto material and config transactions
mkdir -p config
rm -fr config/*
rm -fr crypto-config/*

# Generate crypto material
cryptogen generate --config=./crypto-config.yaml
if [ "$?" -ne 0 ]; then
  echo "Failed to generate crypto material..."
  exit 1
fi

# Generate genesis block for orderer
configtxgen -profile OrdererGenesis -channelID system-channel -outputBlock ./config/genesis.block
if [ "$?" -ne 0 ]; then
  echo "Failed to generate orderer genesis block..."
  exit 1
fi

# Generate channel creation transaction
configtxgen -profile CbtChannel -outputCreateChannelTx ./config/$CHANNEL_NAME.tx -channelID $CHANNEL_NAME
if [ "$?" -ne 0 ]; then
  echo "Failed to generate channel creation transaction..."
  exit 1
fi

# Generate anchor peer transaction for nbrb
configtxgen -profile CbtChannel -outputAnchorPeersUpdate ./config/nbrbMSPanchors.tx -channelID $CHANNEL_NAME -asOrg nbrbMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for nbrbMSP..."
  exit 1
fi

# Generate anchor peer transaction for bank-alfa
configtxgen -profile CbtChannel -outputAnchorPeersUpdate ./config/alfaMSPanchors.tx -channelID $CHANNEL_NAME -asOrg bank-alfaMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for bank-alfaMSP..."
  exit 1
fi

# Generate anchor peer transaction for bank-vtb
configtxgen -profile CbtChannel -outputAnchorPeersUpdate ./config/vtbMSPanchors.tx -channelID $CHANNEL_NAME -asOrg bank-vtbMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for bank-vtb..."
  exit 1
fi

# Generate anchor peer transaction for sber
configtxgen -profile CbtChannel -outputAnchorPeersUpdate ./config/sberMSPanchors.tx -channelID $CHANNEL_NAME -asOrg bank-sberMSP
if [ "$?" -ne 0 ]; then
  echo "Failed to generate anchor peer update for bank-sberMSP..."
  exit 1
fi